import SubSection from './SubSection';

export default interface Section {
    id: string,
    title: string,
    subSections: Array<SubSection>
};
