import StepDep from './StepDep';

export default interface StepProps {
    value: any,
    onChange: (value: any, provides: Array<StepDep>) => void,
};
