import StepDep from './StepDep';
import StepProps from './StepProps';

export default interface Step {
    component: React.FC<StepProps>,
    route: string,
    requires: Array<StepDep>
};
