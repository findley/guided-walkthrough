import Step from './Step';

export default interface SubSection {
    id: string,
    title?: string,
    steps: Array<Step>
};
