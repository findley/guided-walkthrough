import * as Steps from './components/steps'
import StepDep from './model/StepDep';
import Section from './model/Section';

export const sections: Array<Section> = [
    {
        id: 'general-company-questions',
        title: 'General Company Questions',
        subSections: [{
            id: 'general-company-questions-sub',
            steps:
            [
                {
                    component: Steps.General.HaveExemptEmployees,
                    route: '/general/have-exempt-employees',
                    requires: [],
                },
                {
                    component: Steps.General.UseWeightedOvertime,
                    route: '/general/use-weighted-overtime',
                    requires: [StepDep.ExemptEmployees],
                },
                {
                    component: Steps.General.HaveCompanyHolidays,
                    route: '/general/have-company-holidays',
                    requires: [],
                },
            ],
        }]
    }
];
