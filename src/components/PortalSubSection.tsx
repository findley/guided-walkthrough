import React from 'react';
import SubSection from '../model/SubSection';

interface props {
    subSection: SubSection
};

const PortalSubSection: React.FC<props> = ({subSection}) => {
    const stepComponents = subSection.steps.map(step => (
        <PortalStep step={step} />
    ));

    return (
        <div id={subSection.id} className="portal-sub-section">
            {stepComponents}
        </div>
    );
};

export default PortalSubSection;
