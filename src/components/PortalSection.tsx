import React from 'react';
import Section from '../model/Section';

interface props {
    section: Section
};

const PortalSection: React.FC<props> = ({section}) => {
    const subSectionComponents = section.subSections.map(subSection => (
        <PortalSubSection subSection={subSection} />
    ));

    return (
        <div id={section.id} className="portal-section">
            {subSectionComponents}
        </div>
    );
};

export default PortalSection;
