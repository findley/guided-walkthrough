import React from 'react';

interface props {
    onNext: () => void,
    onPrev: () => void
};

const StepButtons: React.FC<props> = ({onNext, onPrev}) => {
    return (
        <div>
            <button onClick={onPrev}>Previous</button>
            <button onClick={onNext}>Next</button>
        </div>
    );
};

export default StepButtons;
