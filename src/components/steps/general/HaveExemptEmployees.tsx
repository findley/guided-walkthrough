import React, { useState } from 'react';
import StepProps from '../StepProps';
import StepButtons from '../shared/StepButtons';

const useYesNoRadio = (initalValue: boolean) => {
    const [checked, setChecked] = useState(initalValue);

    const handleChange: any = (event: any) => {
        console.log(event.target.value);
        setChecked(event.target.value === '1');
    };

    return [checked, handleChange];
};

export const HaveExemptEmployees: React.FC<StepProps> = ({value, onNext, onPrev}) => {
    const [checked, handleChange] = useYesNoRadio(value);

    return (
        <div>
            <div>Do you pay exempt employees?</div>
            <div>
                <input type="radio" id="pay-exempt-yes" name="pay-exempt" value="1" checked={value} onChange={handleChange} />
                <label htmlFor="pay-exempt-yes">Yes</label>

                <input type="radio" id="pay-exempt-no" name="pay-exempt" value="0" checked={!value} onChange={handleChange} />
                <label htmlFor="pay-exempt-no">No</label>
            </div>

            <StepButtons onNext={() => onNext(checked, [])} onPrev={() => onPrev(checked, [])} />
        </div>
    );
};
