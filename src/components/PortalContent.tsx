import React from 'react';
import Section from '../model/Section';
import PortalSection from './PortalSection';

interface props {
    sections: Array<Section>
};

const PortalContent: React.FC<props> = ({sections}) => {
    const sectionCompontents = sections.map(section => (
        <PortalSection section={section} />
    ));

    return (
        <div className="portal-content">
            {sectionCompontents}
        </div>
    );
};

export default PortalContent
