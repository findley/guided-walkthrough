import React, { useState } from 'react';
import Step from '../model/Step';
import StepButtons from './steps/shared/StepButtons';
import StepDep from '../model/StepDep';

interface props {
    step: Step,
    value: any,
};

const PortalStep: React.FC<props> = ({step, value}) => {
    const [nextVal, setNextVal] = useState(value);
    const [provides, setProvides] = useState([]);

    const handleNext = () => {

    };

    const handlePrev = () => {

    };

    const handleChange = (v: any, p: Array<StepDep>) => {
        setNextVal(v);
        setProvides(p);
    };

    return (
        <div>
            <step.component value={nextVal} onChange={handleChange} />
            <StepButtons onNext={handleNext} onPrev={handlePrev} />
        </div>
    );
};

export default PortalStep;
