import React from 'react';
import PortalNavigation from './PortalNavigation';
import PortalContent from './PortalContent';
import { sections } from '../data';

const Portal: React.FC<any> = () => {
    return (
        <div className="portal">
            <PortalNavigation />
            <PortalContent sections={sections} />
        </div>
    );
};

export default Portal
