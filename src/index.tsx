import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Portal from './components/Portal';

ReactDOM.render(<Portal />, document.getElementById('root'));
